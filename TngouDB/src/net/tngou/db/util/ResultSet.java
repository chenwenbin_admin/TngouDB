package net.tngou.db.util;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class ResultSet implements Serializable{

	private int status=200;  //返回状态
	private String msg ; //提示，，一般是指错误提示返回
	
	private int size;
	private int total;
	private int start=0;
	private int page;
	private List<Map<String, Object>> list;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	
	
	@Override
	public String toString() {
		String s="----------------------------------------\n"
				+ "status:"+status+"    "
				+ "start:"+start+"   "
				+ "size:"+size+"   "
				+ "page:"+page+"   "
				+ "total:"+total+"  \n"
				+ "msg:"+msg+"\n";
		if(list!=null)
		{
			for (Map<String, Object> map : list) {
				String maps="**************************************\n";
				Set<Entry<String, Object>> sets = map.entrySet();
				for (Entry<String, Object> entry : sets) {
					maps+=entry.getKey()+":"+entry.getValue()+"          \n";
					
				}	
				s+=maps;
			}
			
		}
	     s+="----------------------------------------\n";
		return s;
	}
	
	
	
}
