package net.tngou.db.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.tngou.db.lucene.LuceneManage;
import net.tngou.db.util.I18N;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

/**
 * 
* @ClassName: Netty
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月19日 下午2:41:46
*
 */
public abstract class Netty {
	private static Logger log = LoggerFactory.getLogger(Netty.class);
	protected static EventLoopGroup bossGroup = new NioEventLoopGroup(10);
	protected static EventLoopGroup workerGroup = new NioEventLoopGroup();
	protected static Channel ch =null;
	public abstract void run();
	public static void stop()
	{
		 log.info(I18N.getValue("stop"));
		 bossGroup.shutdownGracefully();
         workerGroup.shutdownGracefully();
		if(ch!=null)ch.close();
	};
	
}
