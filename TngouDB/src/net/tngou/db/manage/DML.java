package net.tngou.db.manage;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import net.tngou.db.entity.Field;
import net.tngou.db.entity.Field.Type;
import net.tngou.db.util.I18N;
import net.tngou.db.util.ResultSet;

/**
 * 数据操作语言（DML）

         修改数据库中的数据，
         包括插入(INSERT)、
         更新(UPDATE)
         和删除(DELETE)

 * @author tngou
 *
 */
public class DML extends SQL{
	
	
	
	
	/**
	 * 
	* @Title: insert
	* @Description: TODO(这里用一句话描述这个方法的作用)
	* @param @return    设定文件
	* @return Response    返回类型
	* @throws
	 */
	public ResultSet	insert() {
		
		String table=request.getParameterString("table");
		
		@SuppressWarnings("unchecked")
		List<Field> list=(List<Field>) request.getParameter("fields");
		luceneManage.add(list , table);
		response.setMsg(I18N.getValue("save"));
		return response;
	}


	public ResultSet delete() {
		String table = request.getParameterString("table");
		
		Field field=(Field) request.getParameter("field");
		
		luceneManage.delete(field, table );
		response.setMsg(I18N.getValue("delete"));
		return response;
	}

	
	public ResultSet update() {
		String table = request.getParameterString("table");
		
		
		Field term = (Field) request.getParameter("term");
		@SuppressWarnings("unchecked")
		List<Field> list=(List<Field>) request.getParameter("fields");
		Field[] updates= (Field[]) list.toArray(new Field[list.size()]);
		
		luceneManage.update(table, term , updates);
		response.setMsg(I18N.getValue("update"));
		return response;
	}

}
