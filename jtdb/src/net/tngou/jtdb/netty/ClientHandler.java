package net.tngou.jtdb.netty;


import java.io.IOException;
import java.util.List;

import net.tngou.db.util.ResultSet;
import net.tngou.db.util.SerializationUtils;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
/**
* Handles a client-side channel.
*/
public class ClientHandler extends SimpleChannelInboundHandler<byte[]> {
	
	private ResultSet response =null;
	
	
	 @Override
	 protected void messageReceived(ChannelHandlerContext ctx, byte[] msg) 
			 throws IOException {
		 
			this.response= (ResultSet) SerializationUtils.deserialize(msg);
//			System.err.println(response.getMsg());
			
		
		
	 }
	 
	public ResultSet getResponse() {
		
		return this.response;
	}
	 
	
	public void clearResponse() {
		this.response=null;
	}
	 @Override
	 public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		 cause.printStackTrace();
		 ctx.close();
	 }
}