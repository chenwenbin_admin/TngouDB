package net.tngou.jtdb;


/**
 *  
* @ClassName: Field
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou.ceo@aliyun.com
* @date 2014年12月26日 上午9:21:02
*
 */
public class Field {

	private String name;  
	private String value;
	private Type type;
	
	
	public Field(String name,String value,Type type) {
		this.name=name;
		this.value=value;
		this.type=type;
	}
	public Field(String name,String value) {
		this.name=name;
		this.value=value;
		this.type=Type.String;
	}
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}



	public Type getType() {
		return type;
	}



	public void setType(Type type) {
		this.type = type;
	}


	@Override
	public String toString() {
		String s="name:"+name+"\n"
				+"value:"+value+"\n"
				+"type:"+type+"\n";
		
		return s;
	}
	
	/**
	 * 
	* @ClassName: Type
	* @Description: TODO(这里用一句话描述这个类的作用)
	* @author tngou.ceo@aliyun.com
	* @date 2014年12月11日 上午11:07:54
	*
	 */

  public enum Type{
	  //String 不分词  ，Text 分词 ， Key 主键 
		Key,String,Text
	}
}
